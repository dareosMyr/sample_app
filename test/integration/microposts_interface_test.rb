require 'test_helper'

class MicropostsInterfaceTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:test_user)
  end

  test 'micropost interface' do
    log_in_as(@user)
    get root_path
    assert_select 'div.pagination'
    assert_select 'input[type=file]'
    #invalid post
    assert_no_difference 'Micropost.count' do
      post microposts_path, params: { micropost: { content: ''} }
    end
    assert_select 'div#error_explanation'
    #valid post
    content = 'A post to test the efficacy of posting.'
    picture = fixture_file_upload('test/fixtures/black_hat.png', 'image/png')
    assert_difference 'Micropost.count', 1 do
      post microposts_path, params: { micropost: { content: content, picture: picture } }
    end
    assert_redirected_to root_url
    assert @user.microposts.first.picture?
    follow_redirect!
    assert_match content, response.body
    #delete post
    assert_select 'a', text: 'delete'
    first_micropost = @user.microposts.paginate(page: 1).first
    assert_difference 'Micropost.count', -1 do
      delete micropost_path(first_micropost)
    end
    #no delete links on different user
    get user_path(users(:chuck))
    assert_select 'a', text: 'delete', count: 0
  end

  test 'sidebar post count' do
    log_in_as(@user)
    get root_path
    assert_match "#{@user.microposts.count} microposts", response.body
    #no posts
    other_user = users(:nil)
    log_in_as(other_user)
    get root_path
    assert_match '0 microposts', response.body
    other_user.microposts.create!(content: 'A post with content')
    get root_path
    assert_match '1 micropost', response.body
  end
end
